const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());

// Exercice 1.1
app.get('/hello', function (req, res) {
  res.send('Hello World');
})

// Exercice 1.2
app.post('/chat', function (req, res) {
  console.log(req.body);
  if ( req.body.msg == 'ville'){
    res.send('Nous sommes à Paris');
  } else if ( req.body.msg == 'météo'){
    res.send('Il fait beau');
  }  
});


app.listen(process.env.PORT || 3000, function () {
  console.log('Hello World');
})


/**
 * ça c'est juste pour moi pour pas oublier
 * 
 * cd /mnt
 * git add Server.js 
 * git commit -m 
 * git push heroku master
 */
